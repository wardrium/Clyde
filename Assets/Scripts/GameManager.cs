﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public enum Tile { NORMAL, NONE, END, LEFT, RIGHT, DELAY, SPEED, REVERSE}
public enum GameState {PLAN, ACTION, END, STUCK}

public class GameManager : MonoBehaviour {
    static public GameManager s;

    public bool leftTileEnabled = true;
    public bool rightTileEnabled = true;
    public bool reverseTileEnabled = true;
    public bool delayTileEnabled = true;
    public bool speedTileEnabled = true;
    public bool energyEnabled = true;

    public GameObject gridObject;
    public GameObject energyBar;
    public GameObject battery;
    public GameObject shield;
    public GameObject tutorialObject;
    bool tutorial = false;
    List<GameObject> tutorialTips;
    int tutorialTipsIndex = 0;
    RectTransform energyBarGreenTransform;
    Vector2 energyBarGreenSize;
    Transform energyBarText;
    
    public Material highlightMat;
    GameObject highlightedObject;
    Material originalMat;   // Used for tiles.

    public GameObject[] selectionTiles;
    public Material[] selectionTilesMaterials;
    int selectionTilesIndex = 0;

    public GameState state;
    public float maxEnergy = 25f;
    public float startEnergy;
    float energy;

    Dictionary<Vector3, Tile> grid;
    Ray ray;
    RaycastHit hit;
    int numButtonPresses = 0;
    int numActionTotal;  // Number of objects whose Action function is called from NextAction.
    int numActionFinished;  //Number of objects who have finished their action.

    public Text actionButtonText;

    [HideInInspector]
    public List<Enemy> enemies;    // List of enemies that need to be reset when restarting level.
    public List<PlayerController> clydes; // List of Clydes that need to be reset when restarting level.
    public List<Vector3> shields;   // List of shield items that need to be reset when restarting level.
    public List<Vector3> batteries; // List of battery items that need to be reset when restarting level.
    public List<GameObject> sentries; // List of sentry enemies that need to be reset when restarting level. (Only because sentries do not have a script)

    void Awake() {
        s = this;
        enemies = new List<Enemy>();
        clydes = new List<PlayerController>();
    }
    
	void Start () {
        state = GameState.PLAN;
        grid = new Dictionary<Vector3, Tile>();
        if (energyEnabled) {
            energy = startEnergy;
            energyBarGreenTransform = energyBar.transform.Find("Green").GetComponent<RectTransform>();
            energyBarGreenSize = energyBarGreenTransform.GetComponent<RectTransform>().sizeDelta;
            energyBarText = energyBar.transform.Find("Text");
            energyBarText.GetComponent<Text>().text = energy + "/" + maxEnergy;
            energyBarGreenTransform.sizeDelta = new Vector2((energy / maxEnergy) * energyBarGreenSize.x, energyBarGreenSize.y);
        }

        foreach (Transform child in gridObject.transform){
            Vector3 pos = new Vector3(child.position.x, child.position.y, child.position.z);
            if (grid.ContainsKey(pos)) {
                Debug.Log("Error: Duplicate tile at: " + pos);
            }
            else {
                Tile type;
                switch (child.tag) {
                    case "Start":
                        type = Tile.NORMAL;
                        break;
                    case "Normal":
                        type = Tile.NORMAL;
                        break;
                    case "End":
                        type = Tile.END;
                        break;
                    case "Delay":
                        type = Tile.DELAY;
                        break;
                    case "Speed":
                        type = Tile.SPEED;
                        break;
                    default:
                        type = Tile.NORMAL;
                        break;
                }

                grid.Add(pos, type);
            }
        }

        if (tutorialObject != null) {
            tutorial = true;
            tutorialTips = new List<GameObject>();
            foreach (Transform child in tutorialObject.transform) {
                tutorialTips.Add(child.gameObject);
            }
            ShowTutorialTip(0);
        }
	}
	
	public Tile GetTile(Vector3 pos){
        if (grid.ContainsKey(pos)) {
            return grid[pos];
        }
        else {
            return Tile.NONE;
        }
    }

    // Used with button.
    public void GameStateButtonPress() {
        numButtonPresses += 1;
        if (numButtonPresses == 1) {
            SetGameState(GameState.ACTION);
            actionButtonText.text = "Reset";
        }
        else if (numButtonPresses == 2) {
            restartCurrentScene();
            numButtonPresses = 0;
            actionButtonText.text = "Action!";
        }
    }

    void restartCurrentScene() {
        for (int i = 0; i < enemies.Count; ++i) {
            enemies[i].Reset();
        }
        for (int i = 0; i < clydes.Count; ++i) {
            clydes[i].Reset();
        }
        for (int i = 0; i < shields.Count; ++i) {
            Instantiate(shield, shields[i], Quaternion.identity);
        }
        shields.Clear();
        for (int i = 0; i < batteries.Count; ++i) {
            Instantiate(battery, batteries[i], Quaternion.Euler(new Vector3(0f, 0f, 90f)));
        }
        batteries.Clear();
        for (int i = 0; i < sentries.Count; ++i) {
            sentries[i].SetActive(true);
        }

        state = GameState.PLAN;
        ChangeEnergy(startEnergy - energy);
    }

    // Call in case manual reset does not work properly.
    void HardResetCurrentScene() {
        int scene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }

    void LoadNextScene() {
        int scene = SceneManager.GetActiveScene().buildIndex + 1;
        SceneManager.LoadScene(scene);
    }

    public void SetGameState(GameState state0) {
        state = state0;
        if (state == GameState.STUCK) {
            SoundManager.s.PlayJingleLose();
        }
        if (state == GameState.END) {
            LoadNextScene();
        }
    }

    void Update() {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (state == GameState.PLAN) {
            if (Physics.Raycast(ray, out hit)) {
                // Check if mouse moved over to a different game object
                if (highlightedObject != hit.transform.gameObject) {
                    // Unhighlight previous object
                    Unhighlight();
                    Highlight(hit.transform.gameObject);
                }
                if (Input.GetMouseButtonDown(0)) {
                    ChangeHighlightedTile(false);
                }
                else if (Input.GetMouseButtonDown(1)) {
                    ChangeHighlightedTile(true);
                }
            }
            else {
                Unhighlight();
            }
        }
        if (Input.GetKeyDown(KeyCode.F1)) {
            HardResetCurrentScene();
        }
        if (leftTileEnabled && Input.GetKeyDown(KeyCode.Alpha1)) {
            ChangeSelectedTile(0);
        }
        if (rightTileEnabled && Input.GetKeyDown(KeyCode.Alpha2)) {
            ChangeSelectedTile(1);
        }
        if (reverseTileEnabled && Input.GetKeyDown(KeyCode.Alpha3)) {
            ChangeSelectedTile(4);
        }
        if (delayTileEnabled && Input.GetKeyDown(KeyCode.Alpha4)) {
            ChangeSelectedTile(2);
        }
        if (speedTileEnabled && Input.GetKeyDown(KeyCode.Alpha5)) {
            ChangeSelectedTile(3);
        }
        if (tutorial && Input.GetKeyDown(KeyCode.LeftArrow)){
            ShowTutorialTip(tutorialTipsIndex - 1);
        }
        if (tutorial && Input.GetKeyDown(KeyCode.RightArrow)){
            ShowTutorialTip(tutorialTipsIndex + 1);
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            SceneManager.LoadScene(0);
        }
    }

    void Highlight(GameObject obj){
        if (obj.layer == LayerMask.NameToLayer("UnlockedTile")) {
            if (selectionTilesIndex != -1) {
                highlightedObject = obj;
                originalMat = obj.GetComponent<Renderer>().material;
                obj.GetComponent<Renderer>().material = highlightMat;
            }
        }
    }

    void Unhighlight() {
        if (highlightedObject != null) {
            if (highlightedObject.layer == LayerMask.NameToLayer("UnlockedTile")) {
                highlightedObject.GetComponent<Renderer>().material = originalMat;
                highlightedObject = null;
            }
        }
    }

    public void ChangeSelectedTile(int index) {
        if (selectionTilesIndex != index) {
            SoundManager.s.PlayMenuNavigate();
            if (selectionTilesIndex != -1)
                selectionTiles[selectionTilesIndex].GetComponent<Image>().enabled = false;
            selectionTilesIndex = index;
            selectionTiles[selectionTilesIndex].GetComponent<Image>().enabled = true;
        }
    }

    void ChangeHighlightedTile(bool remove) {
        if (highlightedObject != null) {
            if (highlightedObject.layer == LayerMask.NameToLayer("UnlockedTile")) {
                Tile newTile = Tile.NONE;
                if (!remove) {
                    switch (selectionTiles[selectionTilesIndex].tag) {
                        case "TurnRight":
                            newTile = Tile.RIGHT;
                            break;
                        case "TurnLeft":
                            newTile = Tile.LEFT;
                            break;
                        case "Delay":
                            newTile = Tile.DELAY;
                            break;
                        case "Speed":
                            newTile = Tile.SPEED;
                            break;
                        case "Reverse":
                            newTile = Tile.REVERSE;
                            break;
                    }
                    Transform face = highlightedObject.transform.Find("Face");
                    face.GetComponent<MeshRenderer>().enabled = true;
                    face.GetComponent<MeshRenderer>().material = selectionTilesMaterials[selectionTilesIndex];
                }
                else {
                    newTile = Tile.NORMAL;
                    Transform face = highlightedObject.transform.Find("Face");
                    face.GetComponent<MeshRenderer>().enabled = false;
                }
                grid[highlightedObject.transform.position] = newTile;

            }
        }
    }

    public void ChangeEnergy(float amount) {
        if (energyEnabled) {
            energy += amount;
            if (energy > maxEnergy)
                energy = maxEnergy;
            energyBarGreenTransform.sizeDelta = new Vector2((energy / maxEnergy) * energyBarGreenSize.x, energyBarGreenSize.y);
            if (energy > 0)
                energyBarText.GetComponent<Text>().text = energy + "/" + maxEnergy;
            else if (energy == 0)
                energyBarText.GetComponent<Text>().text = 0 + "/" + maxEnergy;
            if (energy < 0) {
                SetGameState(GameState.STUCK);
            }
        }
    }

    void ShowTutorialTip(int index) {
        if (index < 0)
            index = 0;
        else if (index >= tutorialTips.Count)
            index = tutorialTips.Count;
        if (tutorialTipsIndex < tutorialTips.Count)
            tutorialTips[tutorialTipsIndex].SetActive(false);
        tutorialTipsIndex = index;
        if (tutorialTipsIndex < tutorialTips.Count)
            tutorialTips[tutorialTipsIndex].SetActive(true);
    }
}
