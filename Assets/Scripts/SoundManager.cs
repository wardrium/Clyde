﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
    static public SoundManager s;

    AudioSource jingleWin;
    AudioSource jingleLose;
    AudioSource explosion;
    AudioSource menuNavigate;
    AudioSource heroDeath;
    AudioSource pickup;
    
    void Awake() {
        if (s == null) {
            s = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else {
            Destroy(this.gameObject);
        }
    }

    void Start() {
        jingleWin = this.transform.Find("JingleWin").gameObject.GetComponent<AudioSource>();
        jingleLose = this.transform.Find("JingleLose").gameObject.GetComponent<AudioSource>();
        explosion = this.transform.Find("Explosion").gameObject.GetComponent<AudioSource>();
        menuNavigate = this.transform.Find("MenuNavigate").gameObject.GetComponent<AudioSource>();
        heroDeath = this.transform.Find("HeroDeath").gameObject.GetComponent<AudioSource>();
        pickup = this.transform.Find("Pickup").gameObject.GetComponent<AudioSource>();
    }

    public void PlayJingleWin() {
        jingleWin.Play();
    }
    public void PlayJingleLose() {
        jingleLose.Play();
    }
    public void PlayExplosion() {
        explosion.Play();
    }
    public void PlayMenuNavigate() {
        menuNavigate.Play();
    }
    public void PlayHeroDeath() {
        heroDeath.Play();
    }
    public void PlayPickup() {
        pickup.Play();
    }
}
