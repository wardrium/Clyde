﻿using UnityEngine;
using System.Collections;

public enum PlayerState {IDLE, MOVING, DELAYED}
public enum Direction {NORTH, EAST, SOUTH, WEST}

public class PlayerController : MonoBehaviour {  
    public float movementTime = 0.5f;  // Time it takes to move one tile.
    public Direction playerDirection; // Initialize to initial player direction.
    public Material normalMat;      // Normal color of Clyde.
    public Material shieldAvailMat; // Color of Clyde when shield is available.

    private PlayerState playerState;

    // Used for interpolating movement
    private Vector3 startPosition;
    private Vector3 endPosition;
    private float currentTime;

    private bool delayed = true;
    bool shield = false;
    bool invincible = false;
    float shieldDuration = 1.2f;
    float currentShieldDuration = 0.0f;

    Vector3 startPosition0;
    Vector3 startRotation0;
    Direction playerDirection0;

	void Start () {
        playerState = PlayerState.IDLE;
        GameManager.s.clydes.Add(this);
        startPosition0 = this.transform.position;
        startRotation0 = this.transform.eulerAngles;
        playerDirection0 = playerDirection;
	}

    public void Move() {
        currentTime = 0.0f;
        startPosition = this.transform.position;
        Tile currentTile = GameManager.s.GetTile(new Vector3(this.transform.position.x, 0.0f, this.transform.position.z));
        bool move = true;  // Moving this turn.
        movementTime = 0.5f;
        switch (currentTile) {
            case Tile.NORMAL:
                move = true;
                break;
            case Tile.END:
                SoundManager.s.PlayJingleWin();
                GameManager.s.SetGameState(GameState.END);
                move = false;
                break;
            case Tile.RIGHT:
                GameManager.s.ChangeEnergy(-0.5f);
                move = true;
                if (playerDirection == Direction.NORTH)
                    playerDirection = Direction.EAST;
                else if (playerDirection == Direction.EAST)
                    playerDirection = Direction.SOUTH;
                else if (playerDirection == Direction.SOUTH)
                    playerDirection = Direction.WEST;
                else if (playerDirection == Direction.WEST)
                    playerDirection = Direction.NORTH;
                this.transform.Rotate(0f, 90f, 0f);
                break;
            case Tile.LEFT:
                GameManager.s.ChangeEnergy(-0.5f);
                move = true;
                if (playerDirection == Direction.NORTH)
                    playerDirection = Direction.WEST;
                else if (playerDirection == Direction.EAST)
                    playerDirection = Direction.NORTH;
                else if (playerDirection == Direction.SOUTH)
                    playerDirection = Direction.EAST;
                else if (playerDirection == Direction.WEST)
                    playerDirection = Direction.SOUTH;
                this.transform.Rotate(0f, -90f, 0f);
                break;
            case Tile.REVERSE:
                GameManager.s.ChangeEnergy(-1f);
                move = true;
                if (playerDirection == Direction.NORTH)
                    playerDirection = Direction.SOUTH;
                else if (playerDirection == Direction.EAST)
                    playerDirection = Direction.WEST;
                else if (playerDirection == Direction.SOUTH)
                    playerDirection = Direction.NORTH;
                else if (playerDirection == Direction.WEST)
                    playerDirection = Direction.EAST;
                this.transform.Rotate(0f, 180f, 0f);
                break;
            case Tile.DELAY:
                if (delayed)
                    move = false;
                else
                    move = true;
                delayed = !delayed;
                break;
            case Tile.SPEED:
                GameManager.s.ChangeEnergy(-2f);
                movementTime = 0.25f;
                move = true;
                break;
        }

        if (move) {
            playerState = PlayerState.MOVING;
            switch (playerDirection) {
                case Direction.NORTH:
                    endPosition = new Vector3(startPosition.x + 1.0f, startPosition.y, startPosition.z);
                    break;
                case Direction.EAST:
                    endPosition = new Vector3(startPosition.x, startPosition.y, startPosition.z - 1.0f);
                    break;
                case Direction.SOUTH:
                    endPosition = new Vector3(startPosition.x - 1.0f, startPosition.y, startPosition.z);
                    break;
                case Direction.WEST:
                    endPosition = new Vector3(startPosition.x, startPosition.y, startPosition.z + 1.0f);
                    break;
            }

            Tile nextTile = GameManager.s.GetTile(new Vector3(endPosition.x, 0.0f, endPosition.z));

            switch (nextTile) {
                case Tile.NONE:
                    GameManager.s.SetGameState(GameState.STUCK);
                    playerState = PlayerState.IDLE;
                    break;
                default:
                    GameManager.s.ChangeEnergy(-1f);
                    break;
            }
        }
        else {
            playerState = PlayerState.DELAYED;
        }
    }

    void Update() {
        if (GameManager.s.state == GameState.ACTION) {
            if (playerState == PlayerState.IDLE) {
                Move();
            }
            if (playerState == PlayerState.MOVING) {
                currentTime += Time.deltaTime;
                if (currentTime >= movementTime)
                    currentTime = movementTime;
                this.transform.position = Vector3.Lerp(startPosition, endPosition, currentTime / movementTime);
                if (currentTime == movementTime) {
                    playerState = PlayerState.IDLE;
                }
            }
            else if (playerState == PlayerState.DELAYED) {
                currentTime += Time.deltaTime;
                if (currentTime >= movementTime)
                    playerState = PlayerState.IDLE;
            }

            if (invincible) {
                currentShieldDuration += Time.deltaTime;
                if (currentShieldDuration >= shieldDuration) {
                    invincible = false;
                    ((Behaviour)this.gameObject.GetComponent("Halo")).enabled = false;
                }
            }
            if (shield && Input.GetKeyDown(KeyCode.Space)) {
                shield = false;
                this.gameObject.GetComponent<Renderer>().material = normalMat;
                this.gameObject.transform.Find("ClydeDirection").GetComponent<Renderer>().material = normalMat;
                ((Behaviour)this.gameObject.GetComponent("Halo")).enabled = true;
                currentShieldDuration = 0.0f;
                invincible = true;
            }
        }
    }

    void OnTriggerEnter(Collider coll) {
        if (coll.tag == "Enemy" || coll.tag == "Sentry") {
            if (!invincible) {
                SoundManager.s.PlayHeroDeath();
                GameManager.s.SetGameState(GameState.STUCK);
                this.transform.Rotate(0f, 0f, 90f);
            }
            else {
                SoundManager.s.PlayExplosion();
                coll.gameObject.SetActive(false);
                if (coll.tag == "Sentry") {
                    GameManager.s.sentries.Add(coll.gameObject);
                }
            }
        }
        else if (coll.tag == "Battery") {
            SoundManager.s.PlayPickup();
            GameManager.s.ChangeEnergy(10.0f);
            GameManager.s.batteries.Add(coll.transform.position);
            Destroy(coll.gameObject);
        }
        else if (coll.tag == "Shield") {
            SoundManager.s.PlayPickup();
            GameManager.s.shields.Add(coll.transform.position);
            shield = true;
            this.gameObject.GetComponent<Renderer>().material = shieldAvailMat;
            this.gameObject.transform.Find("ClydeDirection").GetComponent<Renderer>().material = shieldAvailMat;
            Destroy(coll.gameObject);
        }
    }

    public void Reset() {
        this.gameObject.SetActive(true);
        if (shield) {
            this.gameObject.GetComponent<Renderer>().material = normalMat;
            this.gameObject.transform.Find("ClydeDirection").GetComponent<Renderer>().material = normalMat;
        }
        if (invincible) {
            ((Behaviour)this.gameObject.GetComponent("Halo")).enabled = false;
        }
        this.transform.position = startPosition0;
        playerDirection = playerDirection0;
        shield = false;
        invincible = false;
        delayed = true;
        playerState = PlayerState.IDLE;
        this.transform.eulerAngles = startRotation0;
        currentTime = 0f;
    }
}
