﻿using UnityEngine;
using System.Collections;

public class SideStepper : Enemy {
    bool moveDir;
    public Vector3 position1;
    public Vector3 position2;

    public override void Start() {
        base.Start();
        moveDir = true;
        position1 = this.transform.position;
        switch (enemyDirection) {
            case Direction.NORTH:
                position2 = new Vector3(position1.x + 1.0f, position1.y, position1.z);
                break;
            case Direction.EAST:
                position2 = new Vector3(position1.x, position1.y, position1.z - 1.0f);
                break;
            case Direction.SOUTH:
                position2 = new Vector3(position1.x - 1.0f, position1.y, position1.z);
                break;
            case Direction.WEST:
                position2 = new Vector3(position1.x, position1.y, position1.z + 1.0f);
                break;
        }
    }

    public override void Move() {
        base.Move();
        currentTime = 0.0f;
        if (moveDir) {
            startPosition = position1;
            endPosition = position2;
        }
        else {
            startPosition = position2;
            endPosition = position1;
        }
        moveDir = !moveDir;
    }

    public override void Reset() {
        base.Reset();
        moveDir = true;
    }
}
