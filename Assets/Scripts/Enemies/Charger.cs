﻿using UnityEngine;
using System.Collections;

public class Charger : Enemy {
    public override void Move() {
        base.Move();
        currentTime = 0.0f;
        startPosition = this.transform.position;
        switch (enemyDirection) {
            case Direction.NORTH:
                endPosition = new Vector3(startPosition.x + 1.0f, startPosition.y, startPosition.z);
                break;
            case Direction.EAST:
                endPosition = new Vector3(startPosition.x, startPosition.y, startPosition.z - 1.0f);
                break;
            case Direction.SOUTH:
                endPosition = new Vector3(startPosition.x - 1.0f, startPosition.y, startPosition.z);
                break;
            case Direction.WEST:
                endPosition = new Vector3(startPosition.x, startPosition.y, startPosition.z + 1.0f);
                break;
        }

        Tile nextTile = GameManager.s.GetTile(new Vector3(endPosition.x, 0.0f, endPosition.z));

        switch (nextTile) {
            case Tile.NONE:
                stuck = true;
                break;
        }
    }
}
