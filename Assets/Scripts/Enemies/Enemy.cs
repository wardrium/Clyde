﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
    public float movementTime = 0.5f;  // Time it takes to move one tile.
    public Direction enemyDirection; // Initialize to initial enemy direction.

    // Used for interpolating movement
    public Vector3 startPosition;
    public Vector3 endPosition;
    public float currentTime = 0f;
    public bool moving = false;
    public bool stuck = false;

    Vector3 startPosition0;
    Direction enemyDirection0;

    public virtual void Start() {
        GameManager.s.enemies.Add(this);
        startPosition0 = this.transform.position;
        enemyDirection0 = enemyDirection;
    }

    public virtual void Update() {
        if (!stuck) {
            if (!moving) {
                if (GameManager.s.state == GameState.ACTION) {
                    Move();
                }
            }
            if (moving) {
                currentTime += Time.deltaTime;
                if (currentTime >= movementTime)
                    currentTime = movementTime;
                this.transform.position = Vector3.Lerp(startPosition, endPosition, currentTime / movementTime);
                if (currentTime == movementTime) {
                    moving = false;
                }
            }
        }
    }
    public virtual void Move() {
        moving = true;
    }

    public virtual void Reset() {
        this.gameObject.SetActive(true);
        this.transform.position = startPosition0;
        enemyDirection = enemyDirection0;
        moving = false;
        stuck = false;
        currentTime = 0f;
    }
}
