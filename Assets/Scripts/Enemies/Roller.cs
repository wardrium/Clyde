﻿using UnityEngine;
using System.Collections;

public class Roller : Enemy {
    public override void Move() {
        base.Move();
        currentTime = 0.0f;
        startPosition = this.transform.position;
        for (int i = 0; i < 2; ++i) {
            switch (enemyDirection) {
                case Direction.NORTH:
                    endPosition = new Vector3(startPosition.x + 1.0f, startPosition.y, startPosition.z);
                    break;
                case Direction.EAST:
                    endPosition = new Vector3(startPosition.x, startPosition.y, startPosition.z - 1.0f);
                    break;
                case Direction.SOUTH:
                    endPosition = new Vector3(startPosition.x - 1.0f, startPosition.y, startPosition.z);
                    break;
                case Direction.WEST:
                    endPosition = new Vector3(startPosition.x, startPosition.y, startPosition.z + 1.0f);
                    break;
            }

            Tile nextTile = GameManager.s.GetTile(new Vector3(endPosition.x, 0.0f, endPosition.z));

            if (nextTile != Tile.NONE) {
                break;
            }
            else {
                switch (enemyDirection) {
                    case Direction.NORTH:
                        enemyDirection = Direction.SOUTH;
                        break;
                    case Direction.EAST:
                        enemyDirection = Direction.WEST;
                        break;
                    case Direction.SOUTH:
                        enemyDirection = Direction.NORTH;
                        break;
                    case Direction.WEST:
                        enemyDirection = Direction.EAST;
                        break;
                }
            }
        }
    }
}
